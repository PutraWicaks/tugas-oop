<?php

    require_once('frog.php');
    require_once('animal.php');
    require_once('ape.php');
    $frog = new Frog("Buduk");
    echo "Name = ". $frog->name ."<br>";
    echo "Legs = ". $frog->legs ."<br>";
    echo "Cold blooded = ". $frog->coldblood ."<br>";
    echo $frog->Jump(). " = Hop Hop"."<br><br>";

    $animal = new Animal("Shaun");
    echo "Name = ". $animal->name ."<br>";
    echo "Legs = ". $animal->legs ."<br>";
    echo "Cold blooded = ". $animal->coldblood ."<br><br>";

    $ape = new Ape("Kera sakti");
    echo "Name = ". $ape->name ."<br>";
    echo "Legs = ". $ape->legs ."<br>";
    echo "Cold blooded = ". $ape->coldblood ."<br>";
    echo $ape->Yell(). " = Auooo ";
?>